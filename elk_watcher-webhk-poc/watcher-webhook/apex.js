const request = require('request');


const sendMessage = (message, channels) => {
    console.log('Sending message to Apex');

    const cb = (err, response, body) => {
        if (err) {
            console.log('Error appeared while sending message', err);
        }
        console.log('Message sent', body);
    };

    const sendRequest = (message) =>
        request({url: "http://172.16.1.3:9696/ords/vacationtrackingtool/webhook/datamonitoring", method: "POST", json: message}, cb);

    if (channels) {
        channels.forEach(channel => {
            message.channel = channel;
            sendRequest(message.hits);
        });
    } else {
        for(let i = 0, l = message.hits.length; i < l; i++)
        sendRequest(message.hits[i]._source);
    }
};

module.exports = sendMessage;
