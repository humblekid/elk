const elasticWatcher = require("elasticsearch-nodejs-watcher");
const sendMessage = require("./apex");

const connection = {
    host: "http://localhost:9200",
    log: "trace"
};

const watcher = {
    schedule: "*/30 * * * * *",
    query: {
        index: 'alert',
        body: {
            query: {
                bool: {
                 //   
                    filter: {
                        range: {"@timestamp": {gte: "now-30s"}}
                    }
                }
                
            }
        }
    },
    predicate: ({hits: {total}}) => total > 0,
    action: sendMessage
};

module.exports = () => elasticWatcher.schedule(connection, watcher);
