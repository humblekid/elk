Common Options - filter:

  The following configuration options are supported by all filter plugins:


    add_field:
      Value type is {logstash-ref}/configuration-file-structure.html#hash[hash]

      Default value is {}

      If this filter is successful, add any arbitrary fields to this event. Field names can be dynamic and include parts of the event using the %{field}.

      Example:

          filter {
            {plugin} {
              add_field => { "foo_%{somefield}" => "Hello world, from %{host}" }
            }
          }
          # You can also add multiple fields at once:
          filter {
            {plugin} {
              add_field => {
                "foo_%{somefield}" => "Hello world, from %{host}"
                "new_field" => "new_static_value"
              }
            }
          }
      If the event has field "somefield" == "hello" this filter, on success, would add field foo_hello if it is present, with the value above and the %{host} piece replaced with that value from the event. The second example would also add a hardcoded field.

    add_tag:

      Value type is {logstash-ref}/configuration-file-structure.html#array[array]

      Default value is []

      If this filter is successful, add arbitrary tags to the event. Tags can be dynamic and include parts of the event using the %{field} syntax.

      Example:

          filter {
            {plugin} {
              add_tag => [ "foo_%{somefield}" ]
            }
          }
          # You can also add multiple tags at once:
          filter {
            {plugin} {
              add_tag => [ "foo_%{somefield}", "taggedy_tag"]
            }
          }
      If the event has field "somefield" == "hello" this filter, on success, would add a tag foo_hello (and the second example would of course add a taggedy_tag tag).

    enable_metric:

      Value type is {logstash-ref}/configuration-file-structure.html#boolean[boolean]

      Default value is true

      Disable or enable metric logging for this specific plugin instance by default we record all the metrics we can, but you can disable metrics collection for a specific plugin.

    id:

      Value type is {logstash-ref}/configuration-file-structure.html#string[string]

      There is no default value for this setting.

      Add a unique ID to the plugin configuration. If no ID is specified, Logstash will generate one. It is strongly recommended to set this ID in your configuration. This is particularly useful when you have two or more plugins of the same type, for example, if you have 2 {plugin} filters. Adding a named ID in this case will help in monitoring Logstash when using the monitoring APIs.

          filter {
            {plugin} {
              id => "ABC"
            }
          }
    
    periodic_flush:
      Value type is {logstash-ref}/configuration-file-structure.html#boolean[boolean]

      Default value is false

      Call the filter flush method at regular interval. Optional.

    remove_field:

      Value type is {logstash-ref}/configuration-file-structure.html#array[array]

      Default value is []

      If this filter is successful, remove arbitrary fields from this event. Fields names can be dynamic and include parts of the event using the %{field} Example:

          filter {
            {plugin} {
              remove_field => [ "foo_%{somefield}" ]
            }
          }
          # You can also remove multiple fields at once:
          filter {
            {plugin} {
              remove_field => [ "foo_%{somefield}", "my_extraneous_field" ]
            }
          }
      If the event has field "somefield" == "hello" this filter, on success, would remove the field with name foo_hello if it is present. The second example would remove an additional, non-dynamic field.

    remove_tag:

      Value type is {logstash-ref}/configuration-file-structure.html#array[array]

      Default value is []

      If this filter is successful, remove arbitrary tags from the event. Tags can be dynamic and include parts of the event using the %{field} syntax.

      Example:

          filter {
            {plugin} {
              remove_tag => [ "foo_%{somefield}" ]
            }
          }
          # You can also remove multiple tags at once:
          filter {
            {plugin} {
              remove_tag => [ "foo_%{somefield}", "sad_unwanted_tag"]
            }
          }
      If the event has field "somefield" == "hello" this filter, on success, would remove the tag foo_hello if it is present. The second example would remove a sad, unwanted tag as well.